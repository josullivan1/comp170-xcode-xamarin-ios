//
//  AppDelegate.m
//  DemoNative
//
//  Created by John O'Sullivan on 10/18/13.
//  Copyright (c) 2013 John O'Sullivan. All rights reserved.
//
// begin-AppDelegate-import
#import "AppDelegate.h"
// end-AppDelegate-import

// begin-AppDelegate-implementation
@implementation AppDelegate
// end-AppDelegate-implementation


// begin-AppDelegate-didFinishLaunchingWithOptions
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    return YES;
}
// end-AppDelegate-didFinishLaunchingWithOptions


// begin-AppDelegate-applicationWillResignActive
- (void)applicationWillResignActive:(UIApplication *)application
{
   
}
// end-AppDelegate-applicationWillResignActive

// begin-AppDelegate-applicationDidEnterBackground
- (void)applicationDidEnterBackground:(UIApplication *)application
{
   
}
// end-AppDelegate-applicationDidEnterBackground

// begin-AppDelegate-applicationWillEnterForeground
- (void)applicationWillEnterForeground:(UIApplication *)application
{
    
}
// end-AppDelegate-applicationWillEnterForeground

// begin-AppDelegate-applicationDidBecomeActive
- (void)applicationDidBecomeActive:(UIApplication *)application
{
   
}
// end-AppDelegate-applicationDidBecomeActive

// begin-AppDelegate-applicationWillTerminate
- (void)applicationWillTerminate:(UIApplication *)application
{
    
}

@end
// end-AppDelegate-applicationWillTerminate
