//
//  LoginController.h
//  DemoNative
//
//  Created by John O'Sullivan on 10/18/13.
//  Copyright (c) 2013 John O'Sullivan. All rights reserved.
//
// begin-login-interface

#import <UIKit/UIKit.h>

@interface LoginController : UIViewController

-(IBAction)dismiss:(id)sender;

@end
// end-login-interface
