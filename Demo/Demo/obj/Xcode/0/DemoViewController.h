// WARNING
// This file has been generated automatically by Xamarin Studio to
// mirror C# types. Changes in this file made by drag-connecting
// from the UI designer will be synchronized back to C#, but
// more complex manual changes may not transfer correctly.


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface DemoViewController : UIViewController {
	UITextField *_btnClickMe;
	UITableView *_tableviewMe;
}

@property (nonatomic, retain) IBOutlet UITextField *btnClickMe;

@property (nonatomic, retain) IBOutlet UITableView *tableviewMe;

- (IBAction)actnButtonClick:(id)sender;

- (IBAction)dismissKeyBoard:(id)sender;

- (IBAction)presentView:(id)sender;

- (IBAction)actnButtonClick2:(id)sender;

@end
